#!/usr/bin/env python3

import os
from sys import argv
import json
from base64 import b64decode

if len(argv) < 2:
    print("Give ejudge mount dir as first arg")
    exit(1)

base_dir = argv[1]
base_dir += '/'
group = next(os.walk(base_dir), (None, None, []))[1]

if len(group) > 0:
    base_dir += group[0]
else:
    print("Error while read ejduge folder")
    exit(1)

base_dir += '/problems/'
problems = next(os.walk(base_dir), (None, None, []))[1]  # [] if no file

out = open('rejects.434', 'w')

for problem in problems:
    sub_path = base_dir + problem + '/runs/'
    runs = next(os.walk(sub_path), (None, None, []))[1]  # [] if no file

    if runs:
        for run in runs:
            if 'Reject' in run:
                reject_path = sub_path + run

                try:
                    file = open(reject_path + '/messages.txt')
                    try:
                        content = file.read()
                        json_reason = json.loads(content)
                        reason = b64decode(json_reason['result']['messages'][0]['content']['data'])
                    except json.decoder.JSONDecodeError:
                        file.close()
                        file = open(reject_path + '/messages.txt') #434 PIDOR
                        content = file.read()
                        json_reason = json.loads(content)
                        reason = b64decode(json_reason['result']['messages'][0]['content']['data'])
                    file.close()
                except FileNotFoundError:
                    reason = b"Subject:\nNo comment\n"

                with open(reject_path + '/source.c', 'r') as src_file:
                    src = src_file.read()
                out.write(reject_path.split('/')[-3])
                out.write('\n')
                out.write(src)
                out.write('\n')
                out.write(reason.decode("utf-8"))
                out.write('\n' + '=' * 100 + '\n')

out.close()
print("Done")
